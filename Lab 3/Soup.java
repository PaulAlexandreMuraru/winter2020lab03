public class Soup {
	//this is about umami/acidity/sweetness. A metric like thos words will be used
	public String taste;
	//density is out of 10. 10 is twice as dense as water, 0 is as dense as water.
	public int density;
	//texture is about if it's cream soup, actual soup, potage, etc...
	public String texture;
	//how much this soup would be rated
	public int stars;
	//this is in degrees celsius
	public int heat;
	
	public void isItHot() {
		if (heat > 50) {
			System.out.println("Yup, it's hot");
		} else if (heat < 26){
			System.out.println("It's cold.");
		} else {
			System.out.println("It's fine.");			
		}
	}
	
	public void isGoodSoup() {
		if (stars >= 3) {
			System.out.println("It should be pretty good");
		} else {
			System.out.println("It's not very popular...");
		}
		System.out.println("It's a "+texture+" kind of soup. It's also "+taste+" in taste.");
		if (density > 4) {
			System.out.println("This soup is pretty dense. It'a good soup.");
		} else {
			System.out.println("It's kind of watery...");			
		}
	}
}