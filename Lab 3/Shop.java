import java.util.Scanner;

public class Shop {
	public static void main(String[] args) {
		Soup[] soupMenu = new Soup[4];
		Scanner scan = new Scanner(System.in);
		for (int i = 0; i < soupMenu.length; i++) {
			soupMenu[i] = new Soup();
			System.out.println("For soup "+(i+1)+", what are the ratings?");
			System.out.println("What's the taste?");
			soupMenu[i].taste = scan.next();
			System.out.println("How dense (out of 10) is it? 10 being twice as dense as water and 0 being as dense as water.");
			soupMenu[i].density = scan.nextInt();
			System.out.println("What's the texture?");
			soupMenu[i].texture = scan.next();
			System.out.println("How many stars?");
			soupMenu[i].stars = scan.nextInt();
			System.out.println("How hot?");
			soupMenu[i].heat = scan.nextInt();
		}
		
		System.out.println("Taste is : " + soupMenu[3].taste);
		System.out.println("Density is : " + soupMenu[3].density);
		System.out.println("texture is : " + soupMenu[3].texture);
		System.out.println("Stars is : " + soupMenu[3].stars);
		System.out.println("Heat is : " + soupMenu[3].heat);
		
		soupMenu[3].isItHot();
	}
}